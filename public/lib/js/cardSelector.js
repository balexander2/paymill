/*global Card, document */

var cardVar = new Card({
    form: document.querySelector('form'),
    container: '.card-wrapper',

    formSelectors: {
        expiryInput: 'input[name="expiryMonth"], input[name="expiryYear"]'
    }
});