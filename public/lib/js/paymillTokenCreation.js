/*global $, Pace, document, paymill, PaymillResponseHandler */
var PAYMILL_PUBLIC_KEY = '18945952877f8f095613d571f137d690';
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

Pace.on("done", function () {
    "use strict";
    $(".loader").fadeOut(250);
});

$(document).ready(function () {
    "use strict";
    /*jslint unparam: true*/
    $("#payment-form").submit(function (event) {
        if (!$("#sub").hasClass('disabled')) {
            var fail = $("#fail"),
                form = $(".form-container");
            form.addClass("hidden");
            fail.addClass("hidden");
            Pace.restart();
            // Deactivate submit button to avoid further clicks
            $('.submit-button').attr("disabled", "disabled");

            paymill.createToken({
                number: $('.card-number').val(),  // required, ohne Leerzeichen und Bindestriche
                exp_month: $('.card-expiry-month').val(),   // required
                exp_year: $('.card-expiry-year').val(),     // required, vierstellig z.B. "2016"
                cvc: $('.card-cvc').val(),                  // required
                amount_int: $('.card-amount-int').val(),    // required, integer, z.B. "15" für 0,15 Euro
                currency: $('.card-currency').val(),    // required, ISO 4217 z.B. "EUR" od. "GBP"
                cardholder: $('.card-holdername').val() // optional
            }, PaymillResponseHandler);                   // Info dazu weiter unten
        }
        return false;
    });
    /*jslint unparam: false*/

    /*jshint unused: true*/
    function PaymillResponseHandler(error, result) {
        var fail = $("#fail"),
            form = $(".form-container"),
            thanks = $("#thanks"),
            token = result.token;
        if (error) {
          // Shows the error above the form
            $(".payment-errors").text(error.apierror);
            $(".submit-button").removeAttr("disabled");
            setTimeout(function () { fail.removeClass("hidden"); }, 700); // show and set the message
            setTimeout(function () { fail.addClass("hidden"); }, 3000); // 3 seconds later, hide and clear the message
            setTimeout(function () { form.removeClass("hidden"); }, 3001);
        } else {
            fail.addClass("hidden");
            $('#paymill_token').val(token);
            token = $('#paymill_token').val();
            $.ajax({
                type: "POST",
                url: "/signup-card/paymill/" + token,
                data:  {token: token},
                success: function (result) {
                    console.log(result);
                }
            });
            // Insert token into form in order to submit to server
            form.append("");
            form.addClass("hidden");
            thanks.removeClass("hidden");
        }
    }
    /*jshint unused: false*/
});