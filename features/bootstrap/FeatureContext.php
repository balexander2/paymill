<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Mink\Exception\UnsupportedDriverActionException;
use Behat\Mink\Driver\GoutteDriver;
use PHPUnit_Framework_Assert as PHPUnit;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends Behat\MinkExtension\Context\MinkContext implements Context, SnippetAcceptingContext
{
   
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }
    
    /**
     * @Then I wait for thank you label to appear
     */
    public function iWaitForThankYouLabelToAppear()
    {
        $this->getSession()->wait(5000,
        "$('#thanks').children().length > 0"
        );
    }

    /**
     * @Then I wait for payment error label to appear
     */
    public function iWaitForPaymentErrorLabelToAppear()
    {
        $this->getSession()->wait(5000,
        "$('#fail').children().length > 0"
        );
    }

    /**
     * @Given I wait for page to load
     */
    public function iWaitForPageToLoad()
    {
        $this->getSession()->wait(5000, '(0 === jQuery.active)');
    }



    

}
