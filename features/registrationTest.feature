Feature:
  In order to prove that registration works as intended
  We want to test the registration on the homepage

@mink:selenium2
Scenario: Registration Test
  Given I am on "/"
  When I fill in "name" with "James Franks"
  And I fill in "email" with "mail08@gmail.com"
  And I fill in "phone" with "(412) 858-2000"
  And I fill in "city" with "Trappe, PA"
  And I press "Register"
  Then I wait
  And I should be on "/thankyou"
  And I should see "Thank you!"
