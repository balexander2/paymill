Feature:
  In order to prove that transaction works as intended
  We want to test the payment form

@javascript
Scenario: Transaction Test
  Given I am on "/signup-card"
  And I wait for page to load
  When I fill in "number" with "4111111111111111"
  And I fill in "cvc" with "123"
  And I fill in "name" with "John Doe"
  And I fill in "expiryMonth" with "12"
  And I fill in "expiryYear" with "2020"
  And I press "Submit"
  Then I wait for thank you label to appear

@javascript
Scenario: Transaction Test Fail
  Given I am on "/signup-card"
  And I wait for page to load
  When I fill in "number" with "4111111511111111"
  And I fill in "cvc" with "123"
  And I fill in "name" with "John Doe"
  And I fill in "expiryMonth" with "12"
  And I fill in "expiryYear" with "2020"
  And I press "Submit"
  Then I wait for payment error label to appear
