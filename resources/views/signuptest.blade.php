<!DOCTYPE html>
<html>
  <head>
    <title>Laravel</title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="http://getbootstrap.com/dist/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('lib/css/centersimple.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('lib/css/paymentInfo.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('lib/css/CardHiddenLoader.css') }}">
    <script src="https://code.jquery.com/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('lib/js/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('lib/js/dist/validator.min.js') }}"></script>
    <script type="text/javascript" src="https://bridge.paymill.com/"></script>
    <script type="text/javascript" src="{{ asset('lib/js/paymillTokenCreation.js') }}"></script>
    <script src="http://getbootstrap.com/dist/js/bootstrap.js"></script>
  </head>
  <body class="container">
      <div class="demo-container"> 
        <div class="form-container active">
          <div class="card-wrapper"></div>
          <form id="payment-form" action="#" data-toggle="validator" method="POST">
            <input class="card-amount-int" type="hidden" value="15" />
            <input class="card-currency" type="hidden" value="EUR" />
            <input type="hidden" id="paymill_token" name="paymill_token">

            <div class="form-group">
              <input class="card-number form-control" name="number" type="text" placeholder="Card number, i.e. '4111 1111 1111 1111'" data-error="Please enter the correct card number, i.e. '4111 1111 1111 1111'" required>
              <div class="help-block with-errors"></div>
            </div>

            <div class="form-group">
              <input class="card-cvc form-control" name="cvc" type="text" placeholder="CVC, i.e. '123'" data-error="Please enter the correct CVC, i.e. '123'" required>
              <div class="help-block with-errors"></div>
            </div>

            <div class="form-group">
              <input class="card-holdername form-control" name="name" type="text" placeholder="Card holder, i.e. 'John Doe'" data-error="Please enter the correct card holder, i.e. 'John Doe'" required>
              <div class="help-block with-errors"></div>
            </div>

            <div class="form-group">
              <input class="card-expiry-month form-control" name="expiryMonth" type="text" placeholder="Expiry month, i.e. '12'" data-error="Please enter the correct expiry month, i.e. '12'" required>
              <div class="help-block with-errors"></div>
             </div>

            <div class="form-group">
              <input class="card-expiry-year form-control" name="expiryYear" type="text" placeholder="Expiry year, i.e. '2020'" data-error="Please enter the correct expiry year, i.e. '2020'" required>
              <div class="help-block with-errors"></div>
            </div>

            <div class="form-group col-md-offset-9">
              <button id="sub" class="submit-button btn btn-primary help-block with-errors" type="submit">Submit</button>
            </div>
          </form>
        </div>
        <div class="col-md-6 col-md-offset-4 hidden" id="thanks">
          <label>Thank you!</label>
        </div>

        <div class="col-md-6 col-md-offset-4 hidden" id="fail">
          <label>Payment error</label>
        </div>

        <div class="loader"></div>
        
      </div>

      
    <script type="text/javascript" src="{{ asset('lib/js/card.js') }}"></script>
    <script type="text/javascript" src="{{ asset('lib/js/cardSelector.js') }}"></script>
  </body>
</html>
