<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Paymill;
use Config;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Token;

class SignupController extends Controller
{
	public function index()
	{
		return view('signuptest');
	}

    public function paymill($token)
    {
    	
     	$apiKey = Config::get('paymill.test.private_key');
		$request = new \Paymill\Request($apiKey);

		$transaction = new \Paymill\Models\Request\Transaction();
		$transaction->setToken($token)
		        ->setAmount(4200)
		        ->setCurrency('EUR')
		        ->setDescription('description example');

		$response = $request->create($transaction);

		$newToken = new Token;
		$newToken->token = $token;
		$newToken->save();
		
	}
}
