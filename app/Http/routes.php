<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::auth();

Route::get('/thankyou', 'HomeController@index');
Route::get('/', 'Auth\AuthController@getRegister');

Route::get('/signup-card', 'SignupController@index');
Route::post('/signup-card/paymill/{token}', 'SignupController@paymill');
