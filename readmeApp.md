In order to run this application you need this software to be installed:
	-docker ( https://docs.docker.com/engine/installation/ )
	-docker-compose ( https://docs.docker.com/compose/install/ )
	-composer ( https://getcomposer.org/doc/00-intro.md#installation-linux-unix-osx )
	-behat ( http://docs.behat.org/en/v2.5/quick_intro.html, https://semaphoreci.com/community/tutorials/getting-started-with-bdd-in-laravel ).
	-selenium ( http://seleniumhq.org/download/ , http://docs.behat.org/en/v2.5/cookbook/behat_and_mink.html#test-in-browser-selenium2-session, http://www.function.fr/docker-with-chrome-and-selenium-and-firefox/ )

01. Go to the app (orbita-line-web-app) root folder
02. Type in the terminal: 'mkdir bootstrap/cache' - create cache folder 
03. Type in the terminal: 'composer install' - this will install all the needed files'n'folders
04. Type in the terminal: 'mkdir -p storage/framework/views' - create 'views' folder
05. Type in the terminal: 'mkdir -p storage/framework/sessions' - create 'sessions' folder
06. Type in the terminal: 'cp .env.example .env' - create .env file
07. Type in the terminal: 'php artisan key:generate' - generate new key 
08. Type in the terminal: 'git clone https://github.com/LaraDock/laradock.git docker' - clone laradock repository
09. Type in the terminal: 'cd docker' - go to docker folder
10. Type in the terminal: 'docker-compose up -d' - this will run docker (in order to stop this type 'docker-compose stop')
11. Type in the terminal: 'cd ../' - go to the root of the app
12. Type in the terminal: 'sudo chmod -R 777 storage && sudo chmod -R 777 bootstrap/cache' - give rights to folders storage and bootstrap/cache
13. Start selenium server 
14. Type 'behat features/signupCard.feature' in terminal - run behat scenario and wait till it finishes.